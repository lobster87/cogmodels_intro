library(tidyverse)
library(rstan)
library(shinystan)
library(patchwork)
theme_set(theme_light())

k <- c(45, 24, 97, 254)
n <- sum(k)
data <- list(k=k, n=n) # To be passed on to Stan

#This version of the model is somewhat fussy and fragile, needs to start the chains in a 'nice' location or it's liable to choke. It's fixable: see https://github.com/stan-dev/example-models/blob/master/Bayesian_Cognitive_Modeling/CaseStudies/MPT/MPT_5_Stan.R for some more complex versions of the same model with much better computational properties.
myinits <- list(
  list(c=.5, r=.5, u=.5),
  list(c=.2, r=.2, u=.2),
  list(c=.8, r=.8, u=.8))

parameters <- c("c", "r", "u")  # Parameters to be monitored

# The following command calls Stan with specific options.
# For a detailed description type "?stan".

samples_1 <- stan(file="MPT1.stan",
                data=data, 
                init=myinits,
                pars=parameters,
                iter=6000, 
                chains=3, 
                thin=1,
                warmup=1000,  # Stands for burn-in; Default = iter/2
                # seed=123  # Setting seed; Default is random seed
                )

stop("Ok to inspect samples 1")
mysamples <- data.frame(extract(samples_1,permute=TRUE))

samples_long <- mysamples%>%select(-lp__)%>%pivot_longer(everything())

posterior_hists <-
(ggplot(samples_long, aes(x=value))+
 geom_histogram())+
    facet_wrap(.~name,scales="free")+ggtitle(paste("observed:",paste(k,collapse=",")))

ggsave(posterior_hists,file="posteriors.png")
##Demo ends here!


##just here to check if this is the same as samples_1, as you'd hope. Looks like it.
samples_custom <- stan(file="MPT1_custom.stan",
                data=data, 
                init=myinits,
                pars=parameters,
                iter=6000, 
                chains=3, 
                thin=1,
                warmup=1000,  # Stands for burn-in; Default = iter/2
                # seed=123  # Setting seed; Default is random seed
                )

##Original script continues with a few more observations.

k <- c(106, 41, 107, 166)
n <- sum(k)
data <- list(k=k, n=n) # To be passed on to Stan

samples_2 <- stan(fit=samples_1,#using an existing stan object avoids recompiling, otherwise the same as passing the original model file again. The samples are not being re-used, despite the variable name, just the model.
                  data=data, 
                  init=myinits,  # If not specified, gives random inits
                  pars=parameters,
                  iter=6000, 
                  chains=3, 
                  thin=1,
                  warmup=1000,  # Stands for burn-in; Default = iter/2
                  # seed=123  # Setting seed; Default is random seed
)

k <- c(243, 64, 65, 48)
n <- sum(k)
data <- list(k=k, n=n) # To be passed on to Stan

samples_6 <- stan(fit=samples_1,   
                  data=data, 
                  init=myinits,  # If not specified, gives random inits
                  pars=parameters,
                  iter=6000, 
                  chains=3, 
                  thin=1,
                  warmup=1000,  # Stands for burn-in; Default = iter/2
                  # seed=123  # Setting seed; Default is random seed
                  )

# Now the values for the monitored parameters are in the "samples" object, 
# ready for inspection.

c1 <- extract(samples_1)$c
r1 <- extract(samples_1)$r
u1 <- extract(samples_1)$u
c2 <- extract(samples_2)$c
r2 <- extract(samples_2)$r
u2 <- extract(samples_2)$u
c6 <- extract(samples_6)$c
r6 <- extract(samples_6)$r
u6 <- extract(samples_6)$u


