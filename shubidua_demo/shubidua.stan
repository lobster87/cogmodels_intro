data{
  int N;
  int obs[N];
}
parameters{
  real<lower=0> lambda;
}
model{
  lambda~cauchy(0,1);
  
  for(i  in 1:N){
    obs[i]~poisson(lambda);
  }
}
generated quantities{
  int prior_sim_obs;
  int post_sim_obs;
  prior_sim_obs = poisson_rng(fabs(cauchy_rng(0,1)));
  post_sim_obs = poisson_rng(lambda);
}
