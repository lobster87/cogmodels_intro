library(tidyverse)
library(rstan)
set.seed(4)
rnd_band <- function(n_years, lambda){
    return(data.frame(year = 1:n_years, releases = rpois(n_years,lambda)))
}
##sim world params
n_bands = 100

min_years = 5
max_years = 40
min_lambda = .5
max_lambda = 5

##run sim
sim_lambda <- runif(n_bands, min_lambda, max_lambda)
sim_years <- base::sample(min_years:max_years, n_bands,replace=TRUE)

if(!("sim_bands.csv"%in%list.files())){
    
sim_recovery <- map_dfr(1:n_bands, function(i){
    print(i)
aband <- rnd_band(sim_years[i],sim_lambda[i])

fit <- stan(file="shubidua.stan",
            data = list(N= nrow(aband),
                        obs = aband$releases),
            chains = 4,
            iter = 2000)

samples <- data.frame(extract(fit, permuted=TRUE))

return(
    data.frame(sim_lambda = sim_lambda[i],
           est_lambda = mean(samples$lambda),
           n_years = sim_years[i])
)
})
write.csv(sim_recovery, file="sim_bands.csv")
}else{
    sim_recovery <- read.csv("sim_bands.csv")
}

band_param_scatter <- ggplot(sim_recovery,aes(x=sim_lambda,y=est_lambda))+geom_point()
ggsave(band_param_scatter,file="band_param_scatter.png")

estgap_years <- ggplot(sim_recovery,aes(x=n_years,y=est_lambda-sim_lambda,color=sim_lambda))+geom_point(size=5)
ggsave(estgap_years,file="estgap_years.png")
