# -*- coding: utf-8 -*-
"""
Created on Sat Mar  6 18:49:14 2021

@author: au467306
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import pymc3 as pm
import arviz as az
import pickle


df = pd.read_csv('shubidua_demo/shubi_processed.csv')
print(df.head())

#%%

with pm.Model() as model:
    lambda1 = pm.HalfCauchy('lambda', beta = 1)
    pm.Poisson('obs', mu = lambda1, observed = df.releases)
    
with model:
    trace = pm.sample(draws = 1000, tune = 1000, chains = 4)
    

#%%

with open('fit_model_pymc3.pckl', 'rb') as handle:
    fit_model = pickle.load(handle)
trace = fit_model[1]
model = fit_model[0]


#%%
with model:
    az.plot_trace(trace, compact = False)

# this looks like multidim implementation
# https://docs.pymc.io/notebooks/survival_analysis.html